<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Notfound extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    private function _render($view, $data = array())
    {

    }

    public function index()
    {
        $this->load->view('404_view');
    }
}
