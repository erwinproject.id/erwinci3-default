<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('api_action')) {
    function api_action($data = [])
    {
        $ci = get_instance();
        $ci->load->database();
        $feedback = [];

        switch ($data['action']) {
            case 'add':
                if (isset($_POST) && !empty($_POST)) {
                    // RUN QUERY
                    $ci->db->insert($data['table'], $data['data']);
                }
                break;
            case 'edit':
                if ($ci->input->post('edit_id')) {
                    $id = (int) $ci->input->post('edit_id');

                    if (isset($_POST) && !empty($_POST)) {

                        // RUN QUERY
                        $ci->db->where($data['column_id'], $id);
                        $ci->db->update($data['table'], $data['data']);
                    }
                }
                break;
            case 'delete':
                if ($id = $ci->input->post('delete_id')) {
                    if (isset($_POST) && !empty($_POST)) {

                        // RUN QUERY
                        $ci->db->where($data['column_id'], $id);
                        $ci->db->delete($data['table']);
                    }
                }
                break;
            default:
                # code...
                break;
        }
        if ($ci->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('feedback')) {
    function feedback($status, $action, $display_name)
    {
        $feedback = [];
        if ($status) {
            $feedback = array(
                "status"    => true,
                "msg"        => ucfirst($action) . " " . $display_name . " berhasil",
            );
        } else {
            $feedback = array(
                "status"    => false,
                "msg"        => ucfirst($action) . " " . $display_name . " gagal",
            );
        }

        echo json_encode($feedback);
    }
}

if (!function_exists('breadcrumb')) {
    function breadcrumb()
    {
        $bc_str = "";
        $uri_str = uri_string();
        $arrBC = explode('/', $uri_str, -1);
        if (count($arrBC) > 1) {
            foreach ($arrBC as $bc) {
                $bc_str .= "<li class='breadcrumb-item'><a href=''>$bc</a></li>";
            }
        } else {
            $str_ = implode("", $arrBC);
            if (preg_match('/_/', implode("", $arrBC))) {
                $bc_str = "<li class='breadcrumb-item'><a href='javascript:void(0)'>" . ucfirst(str_replace('_', ' ', $str_)) . "</a></li>";
            } else {
                $bc_str .= "<li class='breadcrumb-item'><a href='javascript:void(0)'>" . ucfirst(implode("", $arrBC)) . "</a></li>";
            }
        }
        return $bc_str;
    }
}
