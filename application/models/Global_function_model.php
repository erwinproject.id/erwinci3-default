<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Global_function_model extends CI_Model
{

    // master function of select2
    function select2_data($table)
    {
        return $this->db->select('*')->from($table)->get();
    }

    function select2_data_where($table, $target, $value)
    {
        return $this->db->select('*')->from($table)->where($target, $value)->get();
    }

    function grabdata($table)
    {
        return $this->db->select('*')->from($table)->get();
    }

    // get range date
    function weekin($week, $year)
    {
        $last_date =  cal_days_in_month(CAL_GREGORIAN, 12, date('Y'));
        $ret['total_week'] = date("W", strtotime($last_date . '-12-' . $year));
        // $ret['total_week'] = $last_date;
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('d/M/Y');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('d/M/Y');
        return $ret;
    }

    // GET PLANT SPECIFICT
    function get_plant($idp)
    {
        return $this->db->select('*')->from('d_plant')->where('id_plant', $idp)->get();
    }

    // GET WAREHOUSE SPECIFICT
    function get_warehouse($id)
    {
        return $this->db->select('*')->from('d_gudang')->where('id', $id)->get();
    }

    function get_unit($id)
    {
        return $this->db->select('*')->from('subd_unit')->where('id', $id)->get();
    }

    function get_typepart($id)
    {
        return $this->db->select('*')->from('subd_jenispart')->where('id', $id)->get();
    }

    function spesifik_datafromid($table, $id)
    {
        return $this->db->select('*')->from($table)->where('id', $id)->get();
    }


    // TAMBAH DATA DARI EXCEL
    public function tambah_item_excel($table_name, $data)
    {
        return $this->db->insert_batch($table_name, $data);
    }

    // FILTER PART BY DATA
    public function search_datapart($table, $select, $from, $where, $id)
    {
        $query = $this->db->query('SELECT * FROM ' . $table . ' WHERE ' . $table . '.id IN (SELECT ' . $select .  ' FROM ' . $from . ' WHERE ' . $where . ' = ' . $id . ');')
            ->result_array();

        return $query;
    }

    // HAPUS SUB DATA KHUSUS TABLE MERGE
    public function search_datapart_delete($table, $colmaster, $idmaster, $colslave, $idslave)
    {
        $query = $this->db->query('DELETE FROM `' . $table . '` WHERE ' . $colmaster . ' = ' . $idmaster . ' AND ' . $colslave . ' = ' . $idslave . ';');
        return $query;
    }

    // cari data dalam array
    function match_data($search, $arraydata, $column)
    {
        $cari = array_search($search, array_column($arraydata, $column));
        return $arraydata[$cari];
    }

}
