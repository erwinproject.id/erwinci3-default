<?php

use LDAP\Result;

defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Account_management extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect(site_url('auth/login'));
            die();
        }

        if (!$this->ion_auth->is_admin()) {
            redirect(site_url('dashboard'));
            die();
        }
        // $this->load->model('Admin_usermanagement_model');
        $this->load->model('Global_function_model');
    }

    private function _render($view, $data = array())
    {
        $user_data =  $this->ion_auth->user()->row();
        $user_groups = $this->ion_auth->get_users_groups($user_data->id)->result();

        $data['users'] = $user_data;
        $data['usersg'] = $user_groups[0];
        $this->load->view('header', $data);
        $this->load->view('navbar');
        $this->load->view('sidebar', $data);
        $this->load->view($view, $data);
        $this->load->view('footer');
    }

    public function index()
    {
        $data['title']     = "Management Users";
        $data['name_page'] = "Management Users";
        $this->_render('Admin_usermanagement_view', $data);
        $uri = &load_class('URI', 'core');
    }

    #region ==================== USER ACCOUNT
    public function user_account()
    {
        $data['title']     = "User Account";
        $data['name_page'] = "User Account";
        $data['js'] = "account_management/user_account";
        $data['list_group'] = $this->db->get('groups')->result();

        $this->_render('user_account_view', $data);
    }

    public function user_account_action($cmd)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }

        switch ($cmd) {
            case 'edit': // edit
                if ($this->input->post('edit_id')) {
                    $id = $this->input->post('edit_id');
                    $user = $this->ion_auth->user($id)->row();
                    $groups = $this->ion_auth->groups()->result_array();
                    $currentGroups = $this->ion_auth->get_users_groups($id)->result();


                    if (isset($_POST) && !empty($_POST)) {
                        $data = array(
                            'first_name' => $this->input->post('edit_nama'),
                            'username'  => $this->input->post('edit_username'),
                            'email'  => $this->input->post('edit_email'),
                        );

                        // update the password if it was posted
                        if ($this->input->post('edit_password')) {
                            $data['password'] = $this->input->post('edit_password');
                        }

                        // Only allow updating groups if user is admin
                        if ($this->ion_auth->is_admin()) {
                            //Update the groups user belongs to
                            $groupData = $this->input->post('edit_accesslevel');
                            if (isset($groupData) && !empty($groupData)) {
                                $this->ion_auth->remove_from_group('', $id);
                                $this->ion_auth->add_to_group($groupData, $id);
                            }
                        }

                        // check to see if we are updating the user
                        if ($this->ion_auth->update($user->id, $data)) {
                            // redirect them back to the admin page if admin, or to the base url if non admin
                            $feedback = array(
                                "status"    => true,
                                "msg"        => $this->ion_auth->messages(),
                            );
                        } else {
                            // redirect them back to the admin page if admin, or to the base url if non admin
                            $this->session->set_flashdata('message', $this->ion_auth->errors());
                            $feedback = array(
                                "status"    => false,
                                "msg"        =>  $this->ion_auth->errors(),
                            );
                        }

                        echo json_encode($feedback);
                    }
                }
                break;

            case 'add': // add
                $data = array(
                    'first_name' => $this->input->post('add_nama'),
                    'username'  => $this->input->post('add_username'),
                    'email'  => $this->input->post('add_email'),
                    'password'  => $this->input->post('add_password'),
                );

                $email = $this->input->post('add_email');
                $identity = $this->input->post('add_username');
                $password = $this->input->post('add_password');
                $additional_data = array(
                    'first_name' => $this->input->post('add_nama'),
                    'company'    => "PT Karyaindah Alam Sejahtera",
                );
                $new_user = $this->ion_auth->register($identity, $password, $email, $additional_data, $this->input->post('add_accesslevel'));
                if ($new_user) {
                    $feedback = array(
                        "status"    => true,
                        "msg"        => $this->ion_auth->messages(),
                    );
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    $feedback = array(
                        "status"    => false,
                        "msg"        =>  $this->ion_auth->errors(),
                    );
                }

                echo json_encode($feedback);
                break;

            case 'delete': // deleting
                $query = $this->db->delete('users', array('id' => $this->input->post('id')));
                if ($query) {
                    $feedback = array('status' => true, 'msg' => "Success Deleted");
                } else {
                    $feedback = array('status' => false, 'msg' => "Failed Deleted");
                }
                echo json_encode($feedback);
                break;
            default:
                # code...
                break;
        }
    }

    #endregion

    public function user_level()
    {
        $data['title']     = "User Level";
        $data['name_page'] = "User Level";
        $data['js'] = "account_management/userlevel";


        $this->_render('user_level_view', $data);
    }

    public function review_privileges()
    {
        $data['title']     = "Review Privileges";
        $data['name_page'] = "Review Privileges";
        $data['js'] = "account_management/review_privilages";

        $this->_render('review_privileges_view', $data);
    }

    // api data
    public function api($cmd)
    {
        switch ($cmd) {
            case 'table_userlevel': // showing table user level
                $query = $this->db->get('groups')->result_array();
                $obj = array('data' => $query);

                echo json_encode($obj);
                break;

            case 'table_review_privileges': // showing table review privilage
                // $datas = $this->db->query('SELECT tra.ID, tra.user_id, userlist.nama, tra.privilage, tra.kelompok_prv, tra.catatan, lkp.kelompok_prv AS nama_kelompok, lrp.reiew_privilages AS nama_review FROM table_review_access AS tra INNER JOIN table_user_passw AS userlist INNER JOIN list_kelompok_prv AS lkp INNER JOIN list_review_privilage AS lrp WHERE tra.user_id = userlist.ID AND tra.kelompok_prv = lkp.ID AND tra.privilage = lrp.id ORDER BY ID ASC;')->result_array();

                $datas = $this->db->select('tra.ID, tra.user_id, userlist.nama, tra.privilage, tra.kelompok_prv, tra.catatan, lkp.kelompok_prv AS nama_kelompok, lrp.reiew_privilages AS nama_review')
                    ->from('table_review_access AS tra')
                    ->join('table_user_passw AS userlist', 'tra.user_id = userlist.ID')
                    ->join('list_kelompok_prv AS lkp', 'tra.kelompok_prv = lkp.ID')
                    ->join('list_review_privilage AS lrp', 'tra.privilage = lrp.id')
                    ->get()->result_array();

                $obj = array('data' => $datas);
                echo json_encode($obj);
                break;

            case 'table_useraccount': // useraccount
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);
                $datas = $this->db->get('users')->result_array();
                for ($i = 0; $i < count($datas); $i++) {
                    $qs_group = $this->db->select('*')
                        ->from('users_groups ug')
                        ->where('user_id', $datas[$i]['id'])
                        ->join('groups', 'ug.group_id = groups.id', 'left')
                        ->get()->result();


                    foreach ($qs_group as $qg) {
                        $datas[$i]['access_group_id'][] = $qg->group_id;
                        $datas[$i]['access_group_name'][] = $qg->description;
                    }
                }

                $obj = array('data' => $datas);
                echo json_encode($obj);

                break;
        }
    }
}
