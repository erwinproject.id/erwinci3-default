<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Admin_usermanagement_model extends CI_Model
{
    function select2_selected($id)
    {
        $query = $this->db->select('*')
            ->from('groups')
            ->where('id', $id);
        return $query->get();
    }
}
