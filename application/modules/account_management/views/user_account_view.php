    <!-- [ Main Content ] start -->
    <section class="pcoded-main-container">
    	<div class="pcoded-wrapper">
    		<div class="pcoded-content">
    			<div class="pcoded-inner-content">
    				<!-- [ breadcrumb ] start -->
    				<div class="page-header">
    					<div class="page-block">
    						<div class="row align-items-center">
    							<div class="col-md-12">
    								<div class="page-header-title">
    									<h5 class="m-b-10"><?= $name_page ?></h5>
    								</div>
    								<ul class="breadcrumb">
    									<li class="breadcrumb-item"><a href="<?= site_url() ?>"><i class="feather icon-home"></i></a></li>
    									<!-- <li class="breadcrumb-item"><a href="#!">Table</a></li> -->
    									<li class="breadcrumb-item"><a href="#!"><?= $name_page ?></a></li>
    								</ul>
    							</div>
    						</div>
    					</div>
    				</div>
    				<!-- [ breadcrumb ] end -->
    				<div class="main-body">
    					<div class="page-wrapper">
    						<!-- [ Main Content ] start -->
    						<div class="row">
    							<!-- [ HTML5 Export button ] start -->
    							<div class="col-sm-12">
    								<div class="card">
    									<div class="card-header">
    										<h5>List <?= $name_page ?></h5>
    									</div>
    									<div class="card-block">
    										<button type="button" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#modal_add_user"><i class="feather icon-plus"></i>Add User</button>
    										<div class="table-responsive">
    											<table id="list_user_account" class="table table-bordered table-hover nowrap ">
    												<thead>
    													<tr class="text-center">
    														<th>No</th>
    														<th>Nama</th>
    														<th>Email</th>
    														<th>Username</th>
    														<th>Access</th>
    														<th>Action</th>
    													</tr>
    												</thead>
    												<tbody>
    												</tbody>
    											</table>
    										</div>
    									</div>
    								</div>
    							</div>
    							<!-- [ HTML5 Export button ] end -->
    						</div>
    						<!-- [ Main Content ] end -->
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
    <!-- [ Main Content ] end -->


    <!-- MODAL ADD -->
    <div class="modal fade" id="modal_add_user" tabindex="-1" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
    			<form method="post" id="post_add_user" enctype="multipart/form-data" class="form-horizontal">
    				<div class="modal-header">
    					<h5 class="modal-title">Add <?= $name_page ?></h5>
    				</div>
    				<div class="modal-body">
    					<div class="mb-3">
    						<label class="form-label">Nama:</label>
    						<input type="text" class="form-control" name="add_nama" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Email:</label>
    						<input type="text" class="date form-control shipping_date" id="" name="add_email" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Username:</label>
    						<input type="text" class="form-control" name="add_username" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Password:</label>
    						<input type="password" class="form-control" name="add_password" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Access Level:</label>
    						<select class="js-example-basic-multiple col-sm-12" name="add_accesslevel[]" multiple="multiple">
    							<?php
								foreach ($list_group as $lg) {
									echo '<option value="' . $lg->id . '">' . $lg->description . '</option>';
								}
								?>
    						</select>
    					</div>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    					<button type="submit" class="btn btn-primary">Save</button>
    				</div>
    				<!--end row-->
    			</form>
    		</div>
    	</div>
    </div>

    <!-- MODAL edit -->
    <div class="modal fade" id="modal_edit_user" tabindex="-1" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
    			<form method="post" id="post_edit_user" enctype="multipart/form-data" class="form-horizontal">
					<input type="hidden" id="edit_id" name="edit_id">
    				<div class="modal-header">
    					<h5 class="modal-title">edit <?= $name_page ?></h5>
    				</div>
    				<div class="modal-body">
    					<div class="mb-3">
    						<label class="form-label">Nama:</label>
    						<input type="text" class="form-control" name="edit_nama" id="edit_nama" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Email:</label>
    						<input type="text" class="date form-control shipping_date" id="edit_email" name="edit_email" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Username:</label>
    						<input type="text" class="form-control" name="edit_username" id="edit_username" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Password:</label>
    						<input type="password" class="form-control" name="edit_password" id="edit_password">
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Access Level:</label>
    						<select class="col-sm-12" name="edit_accesslevel[]" id="edit_accesslevel" multiple="multiple">
    							<?php
								foreach ($list_group as $lg) {
									echo '<option value="' . $lg->id . '">' . $lg->description . '</option>';
								}
								?>
    						</select>
    					</div>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    					<button type="submit" class="btn btn-primary">Save</button>
    				</div>
    				<!--end row-->
    			</form>
    		</div>
    	</div>
    </div>