    <!-- [ Main Content ] start -->
    <section class="pcoded-main-container">
    	<div class="pcoded-wrapper">
    		<div class="pcoded-content">
    			<div class="pcoded-inner-content">
    				<!-- [ breadcrumb ] start -->
    				<div class="page-header">
    					<div class="page-block">
    						<div class="row align-items-center">
    							<div class="col-md-12">
    								<div class="page-header-title">
    									<h5 class="m-b-10"><?= $name_page ?></h5>
    								</div>
    								<ul class="breadcrumb">
    									<li class="breadcrumb-item"><a href="<?= site_url() ?>"><i class="feather icon-home"></i></a></li>
    									<!-- <li class="breadcrumb-item"><a href="#!">Table</a></li> -->
    									<li class="breadcrumb-item"><a href="#!">Account Management</a></li>
    									<li class="breadcrumb-item"><a href="#!"><?= $name_page ?></a></li>
    								</ul>
    							</div>
    						</div>
    					</div>
    				</div>
    				<!-- [ breadcrumb ] end -->
    				<div class="main-body">
    					<div class="page-wrapper">
    						<!-- [ Main Content ] start -->
    						<div class="row">
    							<!-- [ HTML5 Export button ] start -->
    							<div class="col-sm-12">
    								<div class="card">
    									<div class="card-header">
    										<h5>List <?= $name_page ?></h5>
    									</div>
    									<div class="card-body">
    										<button type="button" class="btn btn-outline-info btn-sm mb-3"><i class="feather icon-plus"></i>Add Group</button>
    										<div class="table-responsive">
    											<table id="list_user_level" class="table table-bordered" style="width:100%;">
    												<thead>
    													<tr class="text-center">
    														<th>No</th>
    														<th>User Level Name</th>
    														<th>Description</th>
    														<th>Action</th>
    													</tr>
    												</thead>
    												<tbody>
    												</tbody>
    											</table>
    										</div>
    									</div>
    								</div>
    							</div>
    							<!-- [ HTML5 Export button ] end -->
    						</div>
    						<!-- [ Main Content ] end -->
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
    <!-- [ Main Content ] end -->


    <!-- MODAL ADD SHIPPING INSTRUCTION -->
    <div class="modal fade" id="modal_addsi" tabindex="-1" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
    			<form method="post" id="post_addsi" enctype="multipart/form-data" class="form-horizontal">
    				<div class="modal-header">
    					<h5 class="modal-title">Add <?= $name_page ?></h5>
    				</div>
    				<div class="modal-body">
    					<div class="mb-3">
    						<label class="form-label">Nomor SI:</label>
    						<input type="text" class="form-control" name="no_si" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Estimasi Tanggal Kirim:</label>
    						<input type="text" class="date form-control shipping_date" id="" name="shipping_date" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Catatan:</label>
    						<input type="text" class="form-control" name="note" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Jumlah Container:</label>
    						<input type="text" class="form-control" name="total_container" require>
    					</div>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    					<button type="submit" class="btn btn-primary">Save</button>
    				</div>
    				<!--end row-->
    			</form>
    		</div>
    	</div>
    </div>

    <!-- MODAL EDIT SHIPPING INSTRUCTION -->
    <div class="modal fade" id="modal_editsi" tabindex="-1" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
    			<form method="post" id="post_editsi" enctype="multipart/form-data" class="form-horizontal">
    				<div class="modal-header">
    					<h5 class="modal-title">Edit <?= $name_page ?></h5>
    				</div>
    				<div class="modal-body">
    					<div class="mb-3">
    						<label class="form-label">Nomor SI:</label>
    						<input type="text" class="form-control" name="no_si" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Estimasi Tanggal Kirim:</label>
    						<input type="text" class="date form-control shipping_date" id="shipping_date" name="shipping_date" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Catatan:</label>
    						<input type="text" class="form-control" name="note" require>
    					</div>
    					<div class="mb-3">
    						<label class="form-label">Jumlah Container:</label>
    						<input type="text" class="form-control" name="total_container" require>
    					</div>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    					<button type="submit" class="btn btn-primary">Save</button>
    				</div>
    				<!--end row-->
    			</form>
    		</div>
    	</div>
    </div>

    <!-- MODAL DELETE SHIPPING INSTRUCTION -->
    <div class="modal fade" id="modal_deletesi" tabindex="-1" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
    			<form method="post" id="post_deletesi" enctype="multipart/form-data" class="form-horizontal">
    				<div class="modal-header">
    					<h5 class="modal-title">Are you sure want to delete?</h5>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    					<button type="submit" class="btn btn-primary">Delete</button>
    				</div>
    				<!--end row-->
    			</form>
    		</div>
    	</div>
    </div>