<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Global_function_model');
        $this->load->model('api_model');
    }

    private function _render($view, $data = array())
    {
        $this->load->view($view, $data);
    }
    // FUNCTION UPLOADING FILE
    function do_upload($container, $allowedtype, $maxsize = 6000)
    {
        $withdate = date('Ymd_');
        if (!empty($_FILES[$container]['name'])) {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = $allowedtype;
            $config['max_size']             = $maxsize;
            $config['encrypt_name']         = true;
            $config['remove_spaces']        = true;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($container)) {
                $feedback = array(
                    'status' => false,
                    'msg' => $this->upload->display_errors()
                );
            } else {
                $gbr =  $this->upload->data();
                // renaming
                rename('./uploads/' . $gbr['file_name'], './uploads/' . $withdate . $gbr['file_name']);
                $gbr['file_name'] = $withdate . $gbr['file_name'];

                $config['image_library'] = 'gd2';
                $config['source_image'] = './uploads/' . $gbr['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                // $config['quality'] = '50%';
                $config['width'] = 600;
                // $config['height'] = 400;
                $config['new_image'] = './uploads/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar = $gbr['file_name'];
                $feedback = array(
                    'status' => false,
                    'msg' => $gambar
                );
            }
        }


        return $feedback;
    }

    // remove image
    public function remove_image()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth', 'refresh');
            die();
        }
        $name = $this->input->post('target');
        if ($name) {
            unlink('uploads/' . $name);
        }
    }

    public function index()
    {
        // === MAIN SEQUENCE
        $data['name_page'] = "API";
        // $this->_render('login_view', $data);
    }

    public function convert_select2()
    {
        $table = $this->input->post('table');
        $sid = $this->input->post('sid');
        $sname = $this->input->post('sname');

        $datas = $this->db->get($table)->result_array();
        if (is_array($datas)) {
            for ($i = 0; $i < count($datas); $i++) {
                $result[] = array(
                    'id' => $datas[$i][$sid],
                    'text' => $datas[$i][$sname],
                );
            }

            echo json_encode($result);
        }
    }

    public function callback($cmd, $container)
    {
        switch ($cmd) {
            case 'image':
                // uploading must be login for security reason
                if (!$this->ion_auth->logged_in()) {
                    redirect('auth', 'refresh');
                    die();
                }

                if (!empty($_FILES)) {
                    $result = $this->do_upload($container, 'jpg|png|jpeg');
                } else {
                    $result = array(
                        'status' => false,
                        'msg' => 'File not attach !'
                    );
                }
                break;

            default:
                $result = array(
                    'status' => false,
                    'msg' => 'Wrong Place !!'
                );
                break;
        }

        echo json_encode($result);
    }
}
