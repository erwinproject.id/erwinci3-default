<!DOCTYPE html>
<html lang="en">

<head>
      <title>Change password | DCKIAS</title>
      <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Meta -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="" />
      <meta name="keywords" content="" />
      <meta name="author" content="CodedThemes" />

      <!-- Favicon icon -->
      <link rel="icon" href="<?= base_url() ?>assets/t_dashboard/assets/images/favicon.ico" type="image/x-icon">
      <!-- animation css -->
      <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/animation/css/animate.min.css">
      <!-- fontawesome icon -->
      <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/fonts/fontawesome/css/fontawesome-all.min.css">
      <!-- vendor css -->
      <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/css/style.css">

</head>

<body>
      <div class="auth-wrapper">
            <div class="auth-content">
                  <div class="auth-bg">
                        <span class="r"></span>
                        <span class="r s"></span>
                        <span class="r s"></span>
                        <span class="r"></span>
                  </div>
                  <div class="card">
                        <?php echo form_open("auth/change_password"); ?>
                        <div class="card-body text-center">
                              <h5 class="mb-4">Password</h5>
                              <div class="input-group mb-3">
                                    <?php echo form_input($old_password); ?>
                              </div>
                              <div class="input-group mb-3">
                                    <?php echo form_input($new_password); ?>
                              </div>
                              <div class="input-group mb-4">
                                    <?php echo form_input($new_password_confirm); ?>
                              </div>
                              <?php echo form_input($user_id); ?>
                              <button type="submit" class="btn btn-primary shadow-2 mb-4">Change Password</button>

                        </div>
                        <?php echo form_close(); ?>
                        <a href="<?= base_url() ?>" class="btn">Back To Dashboard</a>
                  </div>
            </div>
      </div>

      <!-- Required Js -->
      <script src="<?= base_url() ?>assets/t_dashboard/assets/js/vendor-all.min.js"></script>
      <script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="<?= base_url() ?>assets/t_dashboard/assets/js/pcoded.min.js"></script>

</body>

</html>