<!-- <h1><?php echo lang('create_user_heading'); ?></h1>
<p><?php echo lang('create_user_subheading'); ?></p>

<div id="infoMessage"><?php echo $message; ?></div>

<?php echo form_open("auth/create_user"); ?>

      <p>
            <?php echo lang('create_user_fname_label', 'first_name'); ?> <br />
            <?php echo form_input($first_name); ?>
      </p>

      <p>
            <?php echo lang('create_user_lname_label', 'last_name'); ?> <br />
            <?php echo form_input($last_name); ?>
      </p>
      
      <?php
      if ($identity_column !== 'email') {
            echo '<p>';
            echo lang('create_user_identity_label', 'identity');
            echo '<br />';
            echo form_error('identity');
            echo form_input($identity);
            echo '</p>';
      }
      ?>

      <p>
            <?php echo lang('create_user_company_label', 'company'); ?> <br />
            <?php echo form_input($company); ?>
      </p>

      <p>
            <?php echo lang('create_user_email_label', 'email'); ?> <br />
            <?php echo form_input($email); ?>
      </p>

      <p>
            <?php echo lang('create_user_phone_label', 'phone'); ?> <br />
            <?php echo form_input($phone); ?>
      </p>

      <p>
            <?php echo lang('create_user_password_label', 'password'); ?> <br />
            <?php echo form_input($password); ?>
      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm'); ?> <br />
            <?php echo form_input($password_confirm); ?>
      </p>


      <p><?php echo form_submit('submit', lang('create_user_submit_btn')); ?></p>

<?php echo form_close(); ?> -->


<!DOCTYPE html>
<html>

<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>DMC | Create New User</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/t_dashboard/plugins/fontawesome-free/css/all.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- icheck bootstrap -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/t_dashboard/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/t_dashboard/dist/css/adminlte.min.css">
      <!-- Google Font: Source Sans Pro -->
      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition register-page">
      <div class="register-box">
            <div class="register-logo">
                  <a href="<?php echo base_url() ?>dashboard/"><b>AINESIA</b> PROJECT</a>
            </div>

            <div class="card">
                  <div class="card-body register-card-body">
                        <p class="login-box-msg">Register a new member</p>
                        <p class="login-box-msg"><?= $message ?></p>

                        <form action="<?php echo base_url() ?>auth/register" id="form_create_user" method="post">

                              <div class="form-group">
                                    <label for="first_name">First Name:</label>
                                    <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Nama depan">

                              </div>
                              <div class="form-group">
                                    <label for="last_name">Last Name:</label>
                                    <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Nama Belakang">
                              </div>
                              <div class="form-group">
                                    <label for="username">Username:</label>
                                    <input type="text" name="identity" class="form-control" id="identity" placeholder="Username">
                              </div>
                              <div class="form-group">
                                    <label for="password">Password:</label>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="enter password">
                              </div>
                              <div class="form-group">
                                    <label for="password_confirm">Confirm Password:</label>
                                    <input type="password" name="password_confirm" class="form-control" id="password_confirm" placeholder="Confirm password">
                              </div>

                              <div class="row">
                                    <div class="col-4">
                                          <button type="button" class="btn btn-secondary btn-block" onclick="goBack()">Back</button>
                                    </div>
                                    <div class="col-4">

                                    </div>
                                    <!-- /.col -->
                                    <div class="col-4">
                                          <button type="submit" class="btn btn-primary btn-block">Register</button>
                                    </div>
                                    <!-- /.col -->
                              </div>
                        </form>
                  </div>
                  <!-- /.form-box -->
            </div><!-- /.card -->
      </div>
      <!-- /.register-box -->

      <!-- jQuery -->
      <script src="<?php echo base_url() ?>assets/t_dashboard/plugins/jquery/jquery.min.js"></script>
      <!-- Bootstrap 4 -->
      <script src="<?php echo base_url() ?>assets/t_dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- AdminLTE App -->
      <script src="<?php echo base_url() ?>assets/t_dashboard/dist/js/adminlte.min.js"></script>
      <script src="<?php echo base_url() ?>assets/t_dashboard/plugins/jquery-validation/jquery.validate.min.js"></script>
      <script src="<?php echo base_url() ?>assets/t_dashboard/plugins/jquery-validation/additional-methods.min.js"></script>
      <script>
            function goBack() {
                  window.history.back();
            }
      </script>
      <script type="text/javascript">
            $(document).ready(function() {
                  // $.validator.setDefaults({
                  //       submitHandler: function() {
                  //             alert("Form successful submitted!");
                  //       }
                  // });
                  $('#form_create_user').validate({
                        rules: {
                              first_name: {
                                    required: true,
                              },
                              username: {
                                    required: true,
                              },
                              password: {
                                    required: true,
                                    minlength: 4
                              },
                              email: {
                                    required: true,
                                    
                              }
                        },
                        messages: {
                              first_name: {
                                    required: "Please enter a first name",
                              },

                              username: {
                                    required: "Please enter a last name",
                              },
                              password: {
                                    required: "Please provide a password",
                                    minlength: "Your password must be at least 4 characters long",

                              },
                        },
                        errorElement: 'span',
                        errorPlacement: function(error, element) {
                              error.addClass('invalid-feedback');
                              element.closest('.form-group').append(error);
                        },
                        highlight: function(element, errorClass, validClass) {
                              $(element).addClass('is-invalid');
                        },
                        unhighlight: function(element, errorClass, validClass) {
                              $(element).removeClass('is-invalid');
                        }
                  });
            });
      </script>
</body>

</html>