<!DOCTYPE html>
<html lang="en">

<head>
  <title>DCKIAS | Karyaindah Alam Sejahtera</title>
  <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
  <!-- Meta -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="Created By Programmer" />
  <meta name="keywords" content="DCKIAS" />
  <meta name="author" content="Erwin, Noval" />

  <!-- Favicon icon -->
  <link rel="icon" href="<?= base_url() ?>assets/t_dashboard/assets/images/favicon.ico" type="image/x-icon">
  <!-- fontawesome icon -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/fonts/fontawesome/css/fontawesome-all.min.css">
  <!-- animation css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/animation/css/animate.min.css">
  <!-- vendor css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/css/style.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/css/layouts/dark.css">

</head>

<body>
  <div class="auth-wrapper aut-bg-img" style="background-image: url('<?= base_url() ?>assets/t_dashboard/assets/images/bg-images/bg3.jpg');">
    <div class="auth-content">
      <div class="text-white">
        <div class="card-body text-center">
          <div class="mb-4">
            <i class="feather icon-unlock auth-icon"></i>
          </div>
          <form id="validation-form123" method="POST" action="<?= site_url('auth/login') ?>">
            <h3 class="mb-4 text-white">Login</h3>
            <div class="input-group mb-3">
              <input type="username" class="form-control" placeholder="Username" name="identity" require>
            </div>
            <div class="input-group mb-4">
              <input type="password" class="form-control" placeholder="password" name="password" require>
            </div>
            <div class="form-group text-left">
              <div class="checkbox checkbox-fill d-inline">
                <input type="checkbox" name="checkbox-fill-1" id="checkbox-fill-a1" checked="">
                <label for="checkbox-fill-a1" class="cr"> Save credentials</label>
              </div>
            </div>
            <button type="submit" class="btn btn-primary shadow-2 mb-4">Login</button>
            <!-- <p class="mb-2 text-muted">Forgot password? <a class="text-white" href="auth-reset-password-v3.html">Reset</a></p>
            <p class="mb-0 text-muted">Don’t have an account? <a class="text-white" href="auth-signup-v3.html">Signup</a></p> -->
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Required Js -->
  <script src="<?= base_url() ?>assets/t_dashboard/assets/js/vendor-all.min.js"></script>
  <script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>assets/t_dashboard/assets/js/pcoded.min.js"></script>

  <script>
    'use strict';
    $(document).ready(function() {
      $(function() {

        // [ Initialize validation ] start
        $('#validation-form123').validate({
          ignore: '.ignore, .select2-input',
          focusInvalid: false,
          rules: {
            'password': {
              required: true,
              minlength: 6,
              maxlength: 20
            },
            'identity': {
              required: true
            },
          },


          errorPlacement: function errorPlacement(error, element) {
            var $parent = $(element).parents('.form-group');

            // Do not duplicate errors
            if ($parent.find('.jquery-validation-error').length) {
              return;
            }

            $parent.append(
              error.addClass('jquery-validation-error small form-text invalid-feedback')
            );
          },
          highlight: function(element) {
            var $el = $(element);
            var $parent = $el.parents('.form-group');

            $el.addClass('is-invalid');

            // Select2 and Tagsinput
            if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
              $el.parent().addClass('is-invalid');
            }
          },
          unhighlight: function(element) {
            $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
          }
        });

      });
    });
  </script>
</body>

</html>