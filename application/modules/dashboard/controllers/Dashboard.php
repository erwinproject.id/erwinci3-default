<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
        if ($this->ion_auth->logged_in()) {
        } else {
            redirect('auth', 'refresh');
        }
    }

    private function _render($view, $data = array())
    {
        $user_data =  $this->ion_auth->user()->row();
        $user_groups = $this->ion_auth->get_users_groups($user_data->id)->result();

        $data['users'] = $user_data;
        $data['usersg'] = $user_groups[0];
        $this->load->view('header', $data);
        $this->load->view('navbar');
        $this->load->view('sidebar');
        $this->load->view($view);
        $this->load->view('footer');
    }

    public function index()
    {
        $data['title']     = "Dashboard";
        $data['name_page'] = "Dashboard";
        // $data['js'] = "dashboard";

        $uri = &load_class('URI', 'core');
        $this->_render('dashboard_view', $data);
    }
}
