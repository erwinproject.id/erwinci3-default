<footer class="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6">
				<script>
					document.write(new Date().getFullYear())
				</script> © DCKIAS.
			</div>
			<div class="col-sm-6">
				<div class="text-sm-end d-none d-sm-block">
					Created with <i class="mdi mdi-heart text-danger"></i> by Planner Engineer KIAS
				</div>
			</div>
		</div>
	</div>
</footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!-- Right Sidebar -->
<div class="right-bar">
	<div data-simplebar class="h-100">
		<div class="rightbar-title d-flex align-items-center px-3 py-4">

			<h5 class="m-0 me-2">Settings</h5>

			<a href="javascript:void(0);" class="right-bar-toggle ms-auto">
				<i class="mdi mdi-close noti-icon"></i>
			</a>
		</div>

		<!-- Settings -->
		<hr class="mt-0" />
		<h6 class="text-center mb-0">Choose Layouts</h6>

		<div class="p-4">
			<div class="mb-2">
				<img src="<?= base_url() ?>assets/t_dashboard/assets/images/layouts/layout-1.jpg" class="img-fluid img-thumbnail" alt="layout-1">
			</div>

			<div class="form-check form-switch mb-3">
				<input class="form-check-input theme-choice" type="checkbox" id="light-mode-switch" checked>
				<label class="form-check-label" for="light-mode-switch">Light Mode</label>
			</div>

			<div class="mb-2">
				<img src="<?= base_url() ?>assets/t_dashboard/assets/images/layouts/layout-2.jpg" class="img-fluid img-thumbnail" alt="layout-2">
			</div>
			<div class="form-check form-switch mb-3">
				<input class="form-check-input theme-choice" type="checkbox" id="dark-mode-switch" data-bsStyle="<?= base_url() ?>assets/css/bootstrap-dark.min.css" data-appStyle="<?= base_url() ?>assets/css/app-dark.min.css">
				<label class="form-check-label" for="dark-mode-switch">Dark Mode</label>
			</div>

			<!-- <div class="mb-2">
				<img src="<?= base_url() ?>assets/t_dashboard/assets/images/layouts/layout-3.jpg" class="img-fluid img-thumbnail" alt="layout-3">
			</div>
			<div class="form-check form-switch mb-5">
				<input class="form-check-input theme-choice" type="checkbox" id="rtl-mode-switch" data-appStyle="assets/css/app-rtl.min.css">
				<label class="form-check-label" for="rtl-mode-switch">RTL Mode</label>
			</div> -->
		</div>

	</div> <!-- end slimscroll-menu-->
</div>
<!-- Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>



<!-- JAVASCRIPT -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/metismenu/metisMenu.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/simplebar/simplebar.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/node-waves/waves.min.js"></script>

<!-- DATERANGE PICKER -->
<script src="<?= base_url() ?>assets/dist/moment.min.js"></script>
<script src="<?= base_url() ?>assets/dist/daterangepicker.min.js"></script>

<!-- SELECT2 -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/select2/js/select2.min.js"></script>

</script>
<!-- apexcharts -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/apexcharts/apexcharts.min.js"></script>

<!-- jquery.vectormap map -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-us-merc-en.js"></script>

<!-- Required datatable js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- Responsive examples -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>


<!-- App js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/js/app.js"></script>

<!-- Each page js -->
<?php if ($js) { ?>
	<!-- <script src="<?= base_url() ?>data/js/<?= $js ?>.js"></script> -->
<?php }; ?>

<style>
	.dataTables_wrapper>.row:nth-child(2) {
		overflow-x: scroll
	}
</style>
<script>
	$(document).ready(function() {
		let t = $('table').DataTable({
			lengthChange: false,
			// scrollX: true
			// scrollX: 1000,
			// scrollCollapse: true 
			columnDefs: [{
				searchable: false,
				// orderable: false,
				targets: 0,
			}, ],
			order: [
				[1, 'asc']
			],
		})

		t.on('order.dt search.dt', function() {
			let i = 1;

			t.cells(null, 0, {
				search: 'applied',
				order: 'applied'
			}).every(function(cell) {
				this.data(i++);
			});
		}).draw();
		// DATATABLE ================

		$('.date').daterangepicker({
			singleDatePicker: true,
			startDate: moment().startOf('hour'),
			endDate: moment().startOf('hour').add(32, 'hour'),
			placeholder: 'Select a Date',
			locale: {
				format: 'YYYY-MM-DD'
			}
		})
		// DATERANGEPICKER =============

		$('.modal select').each(function(id, el) {
			$(el).select2({
				dropdownParent: $(this).parents('.modal'),
				width: 'resolve',
				placeholder: "Select " + $(this).prev().text().split(':').join("")
			})
		})
		// SELECT2 ====================

	})
</script>
</body>


</html>