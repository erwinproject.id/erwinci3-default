<!doctype html>
<html lang="en">

<head>
    <script>
        let home_site = '<?= site_url() ?>';
        let asset_site = '<?= site_url() ?>assets/t_dashboard/';
    </script>
    <meta charset="utf-8" />
    <title><?= $title; ?> | DCKIAS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesdesign" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= base_url() ?>assets/t_dashboard/assets/images/logo-sm.png">

    <!-- jquery.vectormap css -->
    <link href="<?= base_url() ?>assets/t_dashboard/assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />

    <!-- DataTables -->
    <link href="<?= base_url() ?>assets/t_dashboard/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Select2 -->
    <link href="<?= base_url() ?>assets/t_dashboard/assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

    <!-- DateRangePicker -->
    <link href="<?= base_url() ?>assets/dist/daterangepicker.css" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="<?= base_url() ?>assets/t_dashboard/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap Css -->
    <link href="<?= base_url() ?>assets/t_dashboard/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?= base_url() ?>assets/t_dashboard/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?= base_url() ?>assets/t_dashboard/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
</head>