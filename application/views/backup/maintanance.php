
<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--favicon-->
	<link rel="icon" href="http://<?= $_SERVER['HTTP_HOST'] ?>/cmms_eng/assets/t_dashboard/assets/images/favicon-32x32.png" type="image/png" />
	<!-- loader-->
	<link href="http://<?= $_SERVER['HTTP_HOST'] ?>/cmms_eng/assets/t_dashboard/assets/css/pace.min.css" rel="stylesheet" />
	<script src="http://<?= $_SERVER['HTTP_HOST'] ?>/cmms_eng/assets/t_dashboard/assets/js/pace.min.js"></script>
	<!-- Bootstrap CSS -->
	<link href="http://<?= $_SERVER['HTTP_HOST'] ?>/cmms_eng/assets/t_dashboard/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
	<link href="http://<?= $_SERVER['HTTP_HOST'] ?>/cmms_eng/assets/t_dashboard/assets/css/app.css" rel="stylesheet">
	<link href="http://<?= $_SERVER['HTTP_HOST'] ?>/cmms_eng/assets/t_dashboard/assets/css/icons.css" rel="stylesheet">
	<title>Maintanance</title>
</head>

<body class="bg-login">
	<!-- wrapper -->
	<div class="wrapper">
		
		<div class="error-404 d-flex align-items-center justify-content-center">
			<div class="card shadow-none bg-transparent">
				<div class="card-body text-center">
					<h1 class="display-4 mt-5">We are Maintanance!</h1>
					<p>We are currently working hard on this page. please check again later
						<br>to when it'll be live.</p>
					
				</div>
			</div>
		</div>
	</div>
	<!-- end wrapper -->
	<!-- Bootstrap JS -->
	<script src="http://<?= $_SERVER['HTTP_HOST'] ?>/cmms_eng/assets/t_dashboard/assets/js/bootstrap.bundle.min.js"></script>
</body>

</html>