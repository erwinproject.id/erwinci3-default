<?php
// $group = array('admin', 'admin_engineer', 'planner_engineer', 'kabag_engineermtc', 'member');
// $group2 = array('admin', 'admin_engineer', 'planner_engineer', 'kabag_engineermtc');
?>

<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">General</li>
                <li>
                    <a href="<?= base_url() ?>" class="waves-effect">
                        <i class="ri-dashboard-line"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <!-- Stuffing Marsho -->
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-fridge-line"></i>
                        <span>Stuffing Marsho</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?= base_url() ?>stuffing_marsho/shipping_instruction">Marsho Shipping Instruction</a></li>
                        <li><a href="<?= base_url() ?>stuffing_marsho/production_batch">Marsho Production Batch</a></li>
                        <li><a href="<?= base_url() ?>stuffing_marsho/stuffing_container">Marsho Stuffing Container</a></li>
                        <li><a href="<?= base_url() ?>stuffing_marsho/stuffing_batch">Marsho Stuffing Batch</a></li>
                        <li><a href="<?= base_url() ?>stuffing_marsho/batch_return">Marsho Batch Return</a></li>
                        <li><a href="<?= base_url() ?>stuffing_marsho/shipping_breakdown">Shipping Breakdown</a></li>
                        <li><a href="<?= base_url() ?>stuffing_marsho/batch_coa">Marsho Batch CoA</a></li>
                    </ul>
                </li>

                <!-- Stuffing Filling Olein -->
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-oil-line" style="transform:rotate(30deg)"></i>
                        <span>Stuffing Filling Olein</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?= base_url() ?>stuffing_olein/filling_coa">Filling CoA</a></li>
                        <li><a href="<?= base_url() ?>stuffing_olein/production_batch">Filling Production Batch</a></li>
                        <li><a href="<?= base_url() ?>stuffing_olein/preview_si">Filling Preview SI</a></li>
                        <li><a href="<?= base_url() ?>stuffing_olein/shipping_instruction">Filling Shipping Instruction</a></li>
                        <li><a href="<?= base_url() ?>stuffing_olein/filling_stuffing">Filling Stuffing</a></li>
                        <li><a href="<?= base_url() ?>stuffing_olein/stuffing_batch">Filling Stuffing Batch</a></li>
                        <li><a href="<?= base_url() ?>stuffing_olein/stuffing_closing">Filling Stuffing Closing</a></li>
                        <li><a href="<?= base_url() ?>stuffing_olein/stuffing_return">Filling Stuffing return</a></li>
                    </ul>
                </li>

                <!-- PDF Product Review -->
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-file-pdf-line"></i>
                        <span>PDF Product Review</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?= base_url() ?>pdf_product_review/product_review_olein">Product Review Olein</a></li>
                        <li><a href="<?= base_url() ?>pdf_product_review/list_olein_brand">List Olein Brand</a></li>
                        <li><a href="<?= base_url() ?>pdf_product_review/view_prvdf_olein">View PRVDF Olein</a></li>
                    </ul>
                </li>

                <!-- Documented Information -->
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-folder-info-line"></i>
                        <span>Documented Information</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?= base_url() ?>documented_information/standard_operational_procedure">Standard Operational Procedure</a></li>
                        <li><a href="<?= base_url() ?>documented_information/work_instruction">Work Instruction</a></li>
                        <li><a href="<?= base_url() ?>documented_information/working_form">Working Form</a></li>
                        <li><a href="<?= base_url() ?>documented_information/production_criteria">Production Criteria</a></li>
                    </ul>
                </li>

                <!-- Product Review -->
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-eye-line"></i>
                        <span>Product Review</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?= base_url() ?>product_review/shortening">Shortening</a></li>
                        <!-- <li><a href="<?= base_url() ?>product_review/">Shortening (View)</a></li> -->
                        <li><a href="<?= base_url() ?>product_review/shortening_brand">Shortening Brand</a></li>
                        <li><a href="<?= base_url() ?>product_review/bulk_product">Bulk Product</a></li>
                        <li><a href="<?= base_url() ?>product_review/olein">Olein</a></li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">Packaging Data</a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li><a href="<?= base_url() ?>product_review/shortening_plastic_bag">Shortening Plastic Bag</a></li>
                                <li><a href="<?= base_url() ?>product_review/shortening_carton">Shortening Carton</a></li>
                                <li><a href="<?= base_url() ?>product_review/bulk_packaging">Bulk Pakcaging</a></li>
                                <li><a href="<?= base_url() ?>product_review/label">Label</a></li>
                                <li><a href="<?= base_url() ?>product_review/olein_packaging">Olein Packaging</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <!-- Administration -->
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-building-fill"></i>
                        <span>Administration</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?= base_url() ?>administration/divisi">List Divisi</a></li>
                        <li><a href="<?= base_url() ?>administration/penanggung_jawab_dokumen">List Penanggung Jawab Dokumen</a></li>
                        <li><a href="<?= base_url() ?>administration/status_dokumen">List Status Dokumen</a></li>
                        <li><a href="<?= base_url() ?>administration/batch_printing_position">List Batch Printing Position</a></li>
                        <li><a href="<?= base_url() ?>administration/plastic_material">List Plastic Material</a></li>
                        <li><a href="<?= base_url() ?>administration/status_prv">List Status PRV</a></li>
                        <li><a href="<?= base_url() ?>administration/plastic_layer">List Plastic Layer</a></li>
                        <li><a href="<?= base_url() ?>administration/tahapan_spesifik">List Tahapan Spesifik</a></li>
                        <li><a href="<?= base_url() ?>administration/ruang_lingkup_proses">List Ruang Lingkup Proses</a></li>
                        <li><a href="<?= base_url() ?>administration/oil_class">List Oil Class</a></li>
                        <li><a href="<?= base_url() ?>administration/negara">List Negara</a></li>
                        <li><a href="<?= base_url() ?>administration/olein_palletizing">List Olein Palletizing</a></li>
                        <li><a href="<?= base_url() ?>administration/kelompok_prv">List Kelompok PRV</a></li>
                        <li><a href="<?= base_url() ?>administration/review_privilege">List Review Privilege</a></li>
                    </ul>
                </li>

                <!-- Data Informasi Bahan-->
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-mail-send-line"></i>
                        <span>Data Informasi Bahan</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?= base_url() ?>data_informasi_bahan/dokumen_bahan">Dokumen Bahan</a></li>
                        <li><a href="<?= base_url() ?>data_informasi_bahan/produsen_bahan">Produsen Bahan</a></li>
                        <li><a href="<?= base_url() ?>data_informasi_bahan/supplier_bahan">Supplier Bahan</a></li>
                    </ul>
                </li>

                <li class="menu-title">Admin</li>
                <!-- Account Management -->
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-user-settings-line"></i>
                        <span>Account Management</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?= base_url() ?>account_management/user_account">User Account</a></li>
                        <li><a href="<?= base_url() ?>account_management/user_level">User Level</a></li>
                        <li><a href="<?= base_url() ?>account_management/review_privileges">Review Privileges</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->