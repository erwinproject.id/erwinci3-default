<?php
$site = 'http://' . $_SERVER['HTTP_HOST'] . '/ndckias';
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>404 Not Found</title>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="icon" href="<?= $site ?>/assets/images/gawi.png" type="image/x-icon" />
	<!-- Google font-->
	<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="<?= $site ?>/assets/t_dashboard/extra-pages/404/2/css/style.css" />
</head>

<body>
	<div id="container" class="container">
		<ul id="scene" class="scene">
			<li class="layer" data-depth="1.00"><img src="<?= $site ?>/assets/t_dashboard/extra-pages/404/2/images/404-01.png"></li>
			<li class="layer" data-depth="0.60"><img src="<?= $site ?>/assets/t_dashboard/extra-pages/404/2/images/shadows-01.png"></li>
			<li class="layer" data-depth="0.20"><img src="<?= $site ?>/assets/t_dashboard/extra-pages/404/2/images/monster-01.png"></li>
			<li class="layer" data-depth="0.40"><img src="<?= $site ?>/assets/t_dashboard/extra-pages/404/2/images/text-01.png"></li>
			<li class="layer" data-depth="0.10"><img src="<?= $site ?>/assets/t_dashboard/extra-pages/404/2/images/monster-eyes-01.png"></li>
		</ul>
		<!-- <h1>Our Site is Underconstruction - 10 days to go</h1>
		<input type="text" class="form-control"><a href="#!" class="btn search">Search</a>
		<span>or</span> -->
		<a href="<?= $site ?>" class="btn">Back to home</a>
	</div>
	<!-- Scripts -->
	<script src="<?= $site ?>/assets/t_dashboard/extra-pages/404/2/js/parallax.js"></script>
	<script>
		// Pretty simple huh?
		var scene = document.getElementById('scene');
		var parallax = new Parallax(scene);
	</script>

</body>

</html>