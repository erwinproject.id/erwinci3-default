<!-- Required Js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/js/vendor-all.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="<?= base_url() ?>assets/t_dashboard/assets/js/pcoded.min.js"></script>
<!-- amchart js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/amchart/js/amcharts.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/amchart/js/gauge.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/amchart/js/serial.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/amchart/js/light.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/amchart/js/pie.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/amchart/js/ammap.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/amchart/js/usaLow.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/amchart/js/radar.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/amchart/js/worldLow.js"></script>
<!-- notification Js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/notification/js/bootstrap-growl.min.js"></script>
<!-- datatable Js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/data-tables/js/datatables.min.js"></script>
<!-- select2 Js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/select2/js/select2.full.min.js"></script>
<!-- Datepicker Js -->
<script type="text/javascript" src="<?= base_url() ?>assets/dist/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/dist/daterangepicker.min.js"></script>
<!-- notification Js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/notification/js/bootstrap-growl.min.js"></script>
<!-- ekko-lightbox Js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/ekko-lightbox/js/ekko-lightbox.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/lightbox2-master/js/lightbox.min.js"></script>
<script src="<?= base_url() ?>assets/t_dashboard/assets/js/pages/ac-lightbox.js"></script>
<!-- BOOT BOX -->
<script src="<?= base_url() ?>assets/dist/bootbox.min.js"></script>
<!-- sweet alert Js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/sweetalert/js/sweetalert.min.js"></script>
<!-- file-upload Js -->
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<!-- datepicker js -->
<script src="<?= base_url() ?>assets/t_dashboard/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datepicker.min.js"></script>
<!-- main function -->
<script src="<?= base_url() ?>assets/js/main-global.js"></script>

<?php if ($js) { ?>
    <script src="<?= base_url() ?>assets/js/<?= $js ?>.js"></script>
<?php } ?>

</body>

</html>