<!DOCTYPE html>
<html lang="en">

<head>
  <title><?= $title ?> | DCKIAS</title>
  <script>
    let site_url = '<?= site_url() ?>'
  </script>
  <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
  <!-- Meta -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="Document Control Karyaindah Alam Sejahtera" />
  <meta name="keywords" content="DC KIAS">
  <meta name="author" content="integrasi.dev" />

  <!-- Favicon icon -->
  <link rel="icon" href="<?= base_url() ?>assets/t_dashboard/assets/images/gawi.png" type="image/x-icon">
  <!-- fontawesome icon -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/fonts/fontawesome/css/fontawesome-all.min.css">
  <!-- animation css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/animation/css/animate.min.css">
  <!-- notification css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/notification/css/notification.min.css">
  <!-- data tables css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/data-tables/css/datatables.min.css">
  <!-- select2 css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/select2/css/select2.min.css">
  <!-- multi-select css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/multi-select/css/multi-select.css">
  <!-- notification css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/notification/css/notification.min.css">
  <!-- daterangepicker css -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/dist/daterangepicker.css" />
  <!-- dropzone -->
  <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
  <!-- ekko-lightbox css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/ekko-lightbox/css/ekko-lightbox.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/lightbox2-master/css/lightbox.min.css">
  <!-- Bootstrap datetimepicker css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css">
  <!-- vendor css -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/t_dashboard/assets/css/style.css">
  <!-- dark css -->
  <link rel="stylesheet" href="" id="dark_css">
  <style>
    @import url(https://fonts.googleapis.com/css?family=Raleway:400,700);

    .table td {
      padding: 0.3rem;
      /* font-size: 12px; */
      font-family: "Raleway";
    }

    .table th {
      /* font-size: 12px; */
      font-family: "Raleway";
    }

    #preview {
      position: relative;
      width: 50%;
      height: 50%;
      margin: auto;
    }

    body,
    div,
    table {
      font-family: "Raleway";
    }

    .checkbox {
      opacity: 0;
      position: absolute;
    }

    .checkbox-label {
      background-color: lightgray;
      width: 50px;
      height: 26px;
      border-radius: 50px;
      position: relative;
      padding: 5px;
      cursor: pointer;
      display: flex;
      justify-content: space-between;
      align-items: center;
    }

    .fa-moon {
      color: #f1c40f;
    }

    .fa-sun {
      color: #f39c12;
    }

    .checkbox-label .ball {
      background-color: #fff;
      width: 22px;
      height: 22px;
      position: absolute;
      left: 2px;
      top: 2px;
      border-radius: 50%;
      transition: transform 0.2s linear;
    }

    .checkbox:checked+.checkbox-label .ball {
      transform: translateX(24px);
    }

    .td-text-wrap {
      white-space:normal;
      width:250px;
    }

    /* tfoot {
        display: table-header-group;
    } */
  </style>
</head>