<body>
	<!-- [ Pre-loader ] start -->
	<div class="loader-bg">
		<div class="loader-track">
			<div class="loader-fill"></div>
		</div>
	</div>
	<!-- [ Pre-loader ] End -->

	<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar">
		<div class="navbar-wrapper">
			<div class="navbar-brand header-logo">
				<a href="<?= site_url() ?>" class="b-brand">
					<div class="b-bg">
						<i class="fas fa-industry"></i>
					</div>
					<span class="b-title">CASKIAS</span>
				</a>
				<a class="mobile-menu" id="mobile-collapse" href="javascript:void(0)"><span></span></a>
			</div>
			<div class="navbar-content scroll-div">
				<ul class="nav pcoded-inner-navbar">
					<li class="nav-item pcoded-menu-caption">
						<label>Navigation</label>
					</li>
					<li data-username="dashboard" class="nav-item"><a href="<?= base_url('dashboard') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a></li>

					<?php if ($this->ion_auth->is_admin()) { ?>
						<li class="nav-item pcoded-menu-caption">
							<label>Administration Access</label>
						</li>

						<!-- Account Management -->
						<li data-username="Account Management" class="nav-item pcoded-hasmenu">
							<a href="javascript:void(0)" class="nav-link"><span class="pcoded-micon"><i class="feather icon-users"></i></span><span class="pcoded-mtext">Account Management</span></a>
							<ul class="pcoded-submenu">
								<li><a href="<?= base_url() ?>account_management/user_account">User Account</a></li>
								<li><a href="<?= base_url() ?>account_management/user_level">User Level</a></li>
								<li><a href="<?= base_url() ?>account_management/devisi">Devisi</a></li>
							</ul>
						</li>
					<?php } ?>
					
				</ul>
			</div>
		</div>
	</nav>
	<!-- [ navigation menu ] end -->