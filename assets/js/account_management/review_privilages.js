'use strict';
// $(function () {

// });

$(document).ready(function () {
    function dt_userlevel(param) {
        var siteTable = $('#dt_review_privilage').DataTable({
            destroy: true,
            "ajax": site_url + 'account_management/api/table_review_privileges',
            "paging": false,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            "order": [
                [0, "asc"]
            ],
            initComplete: function (data) {
                // this.api().buttons().container()
                //     .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [
                {
                    data: 'ID',
                    className: 'align-middle text-center',
                    width: '1%',
                },
                {
                    data: 'nama',
                    className: 'align-middle',
                    width: '5%',

                },
                {
                    data: 'nama_review',
                    className: 'align-middle  text-center',
                    width: '1%',

                },
                {
                    data: 'nama_kelompok',
                    className: 'align-middle  text-center',
                    width: '1%',

                },
                {
                    data: 'id',
                    className: 'align-middle text-center',
                    width: '1%',
                    "render": function (data, type, row, meta) {
                        let hasil_id = `
                        <div class="btn-group btn-group-sm" role="group" aria-label="button groups sm">
                            <button type="button" class="btn btn-info btn-sm">Edit</button>
                            <button type="button" class="btn btn-danger btn-sm">Hapus</button>
                        </div>
                        `;
                        return hasil_id;
                    }
                },
            ],
        });

        siteTable.on('order.dt search.dt', function () {
            siteTable.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

    dt_userlevel();
});

