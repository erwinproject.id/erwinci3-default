'use strict';

$(document).ready(function () {
    select2s_manual(".js-example-basic-multiple", true, 'Select Access', true)
    select2s_manual("#edit_accesslevel", true, 'Select Access', true)

    // datatable user
    function dt_user_list(param) {
        var siteTable = $('#list_user_account').DataTable({
            destroy: true,
            "ajax": site_url + 'account_management/api/table_useraccount',
            "paging": false,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            "order": [],
            initComplete: function (data) {
                // this.api().buttons().container()
                //     .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [
                {
                    data: 'id',
                    className: 'align-middle text-center',
                    width: '1%',
                },
                {
                    data: 'first_name',
                    className: 'align-middle',
                    width: '5%',

                },
                {
                    data: 'email',
                    className: 'align-middle',
                    width: '1%',

                },
                {
                    data: 'username',
                    className: 'align-middle  text-center',
                    width: '1%',

                },
                {
                    data: 'access_group_name',
                    className: 'align-middle  text-center',
                    width: '1%',

                },
                {
                    data: 'id',
                    className: 'align-middle text-center',
                    width: '1%',
                    "render": function (data, type, row, meta) {
                        let hasil_id = `
                        <div class="btn-group btn-group-sm" role="group" aria-label="button groups sm">
                            <button type="button" class="btn btn-info btn-sm btn-edit">Edit</button>
                            <button type="button" class="btn btn-danger btn-sm btn-hapus">Hapus</button>
                        </div>
                        `;
                        return hasil_id;
                    }
                },
            ],
        });

        $('#list_user_account tbody').on('click', '.btn-edit', function () {
            var data = siteTable.row($(this).parents('tr')).data();
            if (data) {
                $('#edit_id').val(data.id);
                $('#edit_nama').val(data.first_name);
                $('#edit_email').val(data.email);
                $('#edit_username').val(data.username);
                select2_pilih('#edit_accesslevel', data.access_group_id, true);
                $('#modal_edit_user').modal('show');
            }
        });

        $('#list_user_account tbody').on('click', '.btn-hapus', function () {
            var data = siteTable.row($(this).parents('tr')).data();
            if (data) {
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this " + data.first_name + " !",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: site_url + 'account_management/user_account_action/delete',
                                type: "POST",
                                data: {
                                    id: data.id
                                },
                                dataType: "JSON",
                                beforeSend: function () { },
                                success: function (data) {
                                    if (data.status) {
                                        notify('inverse', data.msg);
                                        $('#list_user_account').DataTable().ajax.reload();
                                    } else {
                                        notify('danger', data.msg);
                                    };
                                },
                                error: function () {
                                    notify('danger', 'A Problem Occurs, Please Try Again !!');
                                }
                            });
                        } else {
                            swal("Your " + data.first_name + " is safe!", {
                                icon: "info",
                            });
                        }
                    });
            }
        });
    }
    // call datatable
    dt_user_list();
});

$('#post_add_user').on('submit', function (event) {
    event.preventDefault();
    $.ajax({
        url: site_url + 'account_management/user_account_action/add',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType: "JSON",
        beforeSend: function () { },
        success: function (data) {
            if (data.status) {
                notify('inverse', data.msg);
                $('#list_user_account').DataTable().ajax.reload();
                document.getElementById("post_add_user").reset();
                $('#modal_add_user').modal('hide');

            } else {
                notify('danger', data.msg);
            };
        },
        error: function () {
            notify('danger', 'A Problem Occurs, Please Try Again !!');
        }
    });
});

// post edit user
$('#post_edit_user').on('submit', function (event) {
    event.preventDefault();
    $.ajax({
        url: site_url + 'account_management/user_account_action/edit',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType: "JSON",
        beforeSend: function () { },
        success: function (data) {
            if (data.status) {
                notify('inverse', data.msg);
                $('#list_user_account').DataTable().ajax.reload();
                document.getElementById("post_edit_user").reset();
                $('#modal_edit_user').modal('hide');

            } else {
                notify('danger', data.msg);
            };
        },
        error: function () {
            notify('danger', 'A Problem Occurs, Please Try Again !!');
        }
    });
});