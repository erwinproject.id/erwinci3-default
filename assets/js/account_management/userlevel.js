'use strict';
// $(function () {

// });

$(document).ready(function () {
    function dt_userlevel(param) {
        var siteTable = $('#list_user_level').DataTable({
            destroy: true,
            "ajax": site_url + 'account_management/api/table_userlevel',
            "paging": false,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            "order": [
                [0, "asc"]
            ],
            initComplete: function (data) {
                // this.api().buttons().container()
                //     .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [
                {
                    data: 'id',
                    className: 'align-middle text-center',
                    width: '1%',
                },
                {
                    data: 'name',
                    className: 'align-middle text-center',
                    width: '5%',

                },
                {
                    data: 'description',
                    className: 'align-middle',
                    width: '5%',

                },
                {
                    data: 'id',
                    className: 'align-middle text-center',
                    width: '1%',
                    "render": function (data, type, row, meta) {
                        let hasil_id = `
                        <div class="btn-group btn-group-sm" role="group" aria-label="button groups sm">
                            <button type="button" class="btn btn-info btn-sm">Edit</button>
                            <button type="button" class="btn btn-danger btn-sm">Hapus</button>
                        </div>
                        `;
                        return hasil_id;
                    }
                },
            ],
        });
    }

    dt_userlevel();
});

