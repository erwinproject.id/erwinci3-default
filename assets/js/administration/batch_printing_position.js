function dt_batch_printing_position_list(param) {
    let siteTable = $('#list_batch_printing_position').DataTable({
        destroy: true,
        "ajax": site_url + 'administration/api/table_batch_printing',
        // "paging": false,
        "scrollCollapse": true,
        "scrollY": "500px",
        "scrollX": true,
        "order": [
            [0, "asc"]
        ],
        initComplete: function (data) {
            // this.api().buttons().container()
            //     .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
        }, columns: [
            {
                data: 'ID',
                className: 'align-middle text-center',
                width: '1%',
            },
            {
                data: 'printing_position',
                // className: 'align-middle text-center',
                width: '5%',
            },
            {
                data: 'image',
                // className: 'align-middle text-center',
                width: '5%',
                "render": function (data, type, row, meta) {
                    let lightbox = `<a href="${site_url}uploads/${data}" data-toggle="lightbox" data-title="${row.printing_position}">${data}</a>`;

                    return lightbox;
                }
            },
            {
                data: 'id',
                className: 'align-middle text-center',
                width: '1%',
                "render": function (data, type, row, meta) {
                    let hasil_id = `
                    <div class="btn-group btn-group-sm" role="group" aria-label="button groups sm">
                        <button type="button" class="btn btn-info btn-sm btn-edit" data-toggle="modal" data-target="#modal_edit_batch_printing_position">Edit</button>
                        <button type="button" class="btn btn-danger btn-sm btn-hapus" data-toggle="modal" data-target="#modal_delete_batch_printing_position">Hapus</button>
                    </div>
                    `;
                    return hasil_id;
                }
            },
        ]
    })

    siteTable.on('order.dt search.dt', function () {
        siteTable.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    // INIT DATATABLE ===================


    $("#modal_edit_batch_printing_position").on('show.bs.modal', function (e) {
        let currentBtn = e.relatedTarget
        let data = siteTable.row($(currentBtn).parents('tr')).data();
        if (data) {
            $('#edit_id').val(data.ID);
            $('#edit_printing_position').val(data.printing_position)
            if (data.image) $('#edit_image').siblings('label').text(data.image)
            else $('#edit_image').siblings('label').text('Choose Image')
        }
    })
    // PASS DATA ON EDIT MODAL

    $("#modal_delete_batch_printing_position").on('show.bs.modal', function (e) {
        let currentBtn = e.relatedTarget
        let data = siteTable.row($(currentBtn).parents('tr')).data();

        if (data) {
            $('#delete_id').val(data.ID);
        }
    })
    // PASS DATA ON DELETE MODAL
}

$(document).ready(function () {
    dt_batch_printing_position_list()

    $('#post_add_batch_printing_position').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/batch_printing_position_action/add',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_batch_printing_position').DataTable().ajax.reload();
                    document.getElementById("post_add_batch_printing_position").reset();
                    $('#modal_add_batch_printing_position').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT ADD 

    $('#post_edit_batch_printing_position').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/batch_printing_position_action/edit',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_batch_printing_position').DataTable().ajax.reload();
                    document.getElementById("post_edit_batch_printing_position").reset();
                    $('#modal_edit_batch_printing_position').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT EDIT 

    $('#post_delete_batch_printing_position').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/batch_printing_position_action/delete',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_batch_printing_position').DataTable().ajax.reload();
                    document.getElementById("post_delete_batch_printing_position").reset();
                    $('#modal_delete_batch_printing_position').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT DELETE 


    $('.custom-file-input').on('change', function (e) {
        let filename = this.files[0].name

        $(this).siblings('label').text(filename)
    })
    // CHANGE LABEL FILE INPUT WHEN THERE IS A CHANGE

})