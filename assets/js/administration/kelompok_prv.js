function dt_kelompok_prv_list(param) {
    let siteTable = $('#list_kelompok_prv').DataTable({
        destroy: true,
        "ajax": site_url + 'administration/api/table_kelompok_prv',
        "paging": false,
        "scrollCollapse": true,
        "scrollY": "500px",
        "scrollX": true,
        "order": [
            [0, "asc"]
        ],
        initComplete: function (data) {
            // this.api().buttons().container()
            //     .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
        }, columns: [
            {
                data: 'ID',
                className: 'align-middle text-center',
                width: '1%',
            },
            {
                data: 'kelompok_prv',
                className: 'align-middle text-center',
                width: '5%',
            },
            {
                data: 'id',
                className: 'align-middle text-center',
                width: '1%',
                "render": function (data, type, row, meta) {
                    let hasil_id = `
                    <div class="btn-group btn-group-sm" role="group" aria-label="button groups sm">
                        <button type="button" class="btn btn-info btn-sm btn-edit" data-toggle="modal" data-target="#modal_edit_kelompok_prv">Edit</button>
                        <button type="button" class="btn btn-danger btn-sm btn-hapus" data-toggle="modal" data-target="#modal_delete_kelompok_prv">Hapus</button>
                    </div>
                    `;
                    return hasil_id;
                }
            },
        ]
    })

    siteTable.on('order.dt search.dt', function () {
        siteTable.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    // INIT DATATABLE ===================


    $("#modal_edit_kelompok_prv").on('show.bs.modal', function (e) {
        let currentBtn = e.relatedTarget
        let data = siteTable.row($(currentBtn).parents('tr')).data();
        if (data) {
            $('#edit_id').val(data.ID);
            $('#edit_kelompok_prv').val(data.kelompok_prv)
        }
    })
    // PASS DATA ON EDIT MODAL

    $("#modal_delete_kelompok_prv").on('show.bs.modal', function (e) {
        let currentBtn = e.relatedTarget
        let data = siteTable.row($(currentBtn).parents('tr')).data();

        if (data) {
            $('#delete_id').val(data.ID);
        }
    })
    // PASS DATA ON DELETE MODAL
}

$(document).ready(function () {
    dt_kelompok_prv_list()

    $('#post_add_kelompok_prv').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/kelompok_prv_action/add',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_kelompok_prv').DataTable().ajax.reload();
                    document.getElementById("post_add_kelompok_prv").reset();
                    $('#modal_add_kelompok_prv').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT ADD 

    $('#post_edit_kelompok_prv').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/kelompok_prv_action/edit',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_kelompok_prv').DataTable().ajax.reload();
                    document.getElementById("post_edit_kelompok_prv").reset();
                    $('#modal_edit_kelompok_prv').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT EDIT 

    $('#post_delete_kelompok_prv').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/kelompok_prv_action/delete',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_kelompok_prv').DataTable().ajax.reload();
                    document.getElementById("post_delete_kelompok_prv").reset();
                    $('#modal_delete_kelompok_prv').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT DELETE 
})