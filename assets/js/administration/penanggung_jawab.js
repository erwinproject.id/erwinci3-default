function dt_pjd_list(param) {
    let siteTable = $('#list_pjd').DataTable({
        destroy: true,
        "ajax": site_url + 'administration/api/table_penanggung_jawab',
        "paging": false,
        "scrollCollapse": true,
        "scrollY": "500px",
        "scrollX": true,
        "order": [
            [0, "asc"]
        ],
        initComplete: function (data) {
            // this.api().buttons().container()
            //     .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
        }, columns: [
            {
                data: 'ID',
                className: 'align-middle text-center',
                width: '1%',
            },
            {
                data: 'penanggungjawab',
                // className: 'align-middle text-center',
                width: '5%',
            },
            {
                data: 'id',
                className: 'align-middle text-center',
                width: '1%',
                "render": function (data, type, row, meta) {
                    let hasil_id = `
                    <div class="btn-group btn-group-sm" role="group" aria-label="button groups sm">
                        <button type="button" class="btn btn-info btn-sm btn-edit" data-toggle="modal" data-target="#modal_edit_pjd">Edit</button>
                        <button type="button" class="btn btn-danger btn-sm btn-hapus" data-toggle="modal" data-target="#modal_delete_pjd">Hapus</button>
                    </div>
                    `;
                    return hasil_id;
                }
            },
        ]
    })

    siteTable.on('order.dt search.dt', function () {
        siteTable.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    // INIT DATATABLE ===================


    $("#modal_edit_pjd").on('show.bs.modal', function (e) {
        let currentBtn = e.relatedTarget
        let data = siteTable.row($(currentBtn).parents('tr')).data();
        if (data) {
            $('#edit_id').val(data.ID);
            $('#edit_pjd').val(data.penanggungjawab)
        }
    })
    // PASS DATA ON EDIT MODAL

    $("#modal_delete_pjd").on('show.bs.modal', function (e) {
        let currentBtn = e.relatedTarget
        let data = siteTable.row($(currentBtn).parents('tr')).data();

        if (data) {
            $('#delete_id').val(data.ID);
        }
    })
    // PASS DATA ON DELETE MODAL

    // $('#list_user_account tbody').on('click', '.btn-edit', function () {
    //     var data = siteTable.row($(this).parents('tr')).data();
    //     if (data) {
    //         $('#edit_id').val(data.id);
    //         $('#edit_username').val(data.username);
    //         $('#edit_first_name').val(data.first_name);
    //         select2_pilih('#edit_sdevisi', data.devisi);
    //         select2_pilih('#edit_slevel_access', data.level_id);
    //         $('#modal_edit_user').modal('show');
    //     }
    // });
}

$(document).ready(function () {
    dt_pjd_list()

    $('#post_add_pjd').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/penanggung_jawab_action/add',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_pjd').DataTable().ajax.reload();
                    document.getElementById("post_add_pjd").reset();
                    $('#modal_add_pjd').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT ADD 

    $('#post_edit_pjd').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/penanggung_jawab_action/edit',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_pjd').DataTable().ajax.reload();
                    document.getElementById("post_edit_pjd").reset();
                    $('#modal_edit_pjd').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT EDIT 

    $('#post_delete_pjd').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/penanggung_jawab_action/delete',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_pjd').DataTable().ajax.reload();
                    document.getElementById("post_delete_pjd").reset();
                    $('#modal_delete_pjd').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT DELETE 
})