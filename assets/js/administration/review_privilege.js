function dt_review_privilege_list(param) {
    let siteTable = $('#list_review_privilege').DataTable({
        destroy: true,
        "ajax": site_url + 'administration/api/table_review_privilege',
        "paging": false,
        "scrollCollapse": true,
        "scrollY": "500px",
        "scrollX": true,
        "order": [
            [0, "asc"]
        ],
        initComplete: function (data) {
            // this.api().buttons().container()
            //     .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
        }, columns: [
            {
                data: 'id',
                className: 'align-middle text-center',
                width: '1%',
            },
            {
                data: 'review_privilege',
                className: 'align-middle text-center',
                width: '5%',
            },
            {
                data: 'id',
                className: 'align-middle text-center',
                width: '1%',
                "render": function (data, type, row, meta) {
                    let hasil_id = `
                    <div class="btn-group btn-group-sm" role="group" aria-label="button groups sm">
                        <button type="button" class="btn btn-info btn-sm btn-edit" data-toggle="modal" data-target="#modal_edit_review_privilege">Edit</button>
                        <button type="button" class="btn btn-danger btn-sm btn-hapus" data-toggle="modal" data-target="#modal_delete_review_privilege">Hapus</button>
                    </div>
                    `;
                    return hasil_id;
                }
            },
        ]
    })

    siteTable.on('order.dt search.dt', function () {
        siteTable.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    // INIT DATATABLE ===================


    $("#modal_edit_review_privilege").on('show.bs.modal', function (e) {
        let currentBtn = e.relatedTarget
        let data = siteTable.row($(currentBtn).parents('tr')).data();
        if (data) {
            $('#edit_id').val(data.id);
            $('#edit_review_privilege').val(data.review_privilege)
        }
    })
    // PASS DATA ON EDIT MODAL

    $("#modal_delete_review_privilege").on('show.bs.modal', function (e) {
        let currentBtn = e.relatedTarget
        let data = siteTable.row($(currentBtn).parents('tr')).data();

        if (data) {
            $('#delete_id').val(data.id);
        }
    })
    // PASS DATA ON DELETE MODAL
}

$(document).ready(function () {
    dt_review_privilege_list()

    $('#post_add_review_privilege').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/review_privilege_action/add',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_review_privilege').DataTable().ajax.reload();
                    document.getElementById("post_add_review_privilege").reset();
                    $('#modal_add_review_privilege').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT ADD 

    $('#post_edit_review_privilege').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/review_privilege_action/edit',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_review_privilege').DataTable().ajax.reload();
                    document.getElementById("post_edit_review_privilege").reset();
                    $('#modal_edit_review_privilege').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT EDIT 

    $('#post_delete_review_privilege').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'administration/review_privilege_action/delete',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function () { },
            success: function (data) {
                if (data.status) {
                    notify('inverse', data.msg);
                    $('#list_review_privilege').DataTable().ajax.reload();
                    document.getElementById("post_delete_review_privilege").reset();
                    $('#modal_delete_review_privilege').modal('hide');

                } else {
                    notify('danger', data.msg);
                };
            },
            error: function () {
                notify('danger', 'A Problem Occurs, Please Try Again !!');
            }
        });
    })
    // SUBMIT DELETE 
})