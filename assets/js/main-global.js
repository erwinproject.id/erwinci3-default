// ================== SIDEBAR MENU
const url = window.location.href
const navbarItem = $('.navbar-wrapper li a')
const itemTarget = []

navbarItem.each((id, a) => {
    itemTarget.push(a.href)
})

itemTarget.forEach(a => {
    if (url == a) {
        let li = $('.navbar-wrapper li a[href="' + url + '"]').parent()

        li.addClass('active')
        li.parents('li').addClass('active pcoded-trigger')
    }
});

// DARK MODE SWITCH
function toggleDark(isDark) {
    if (JSON.parse(isDark)) {
        $('#dark_mode').prop('checked', true)
        $('.pcoded-navbar').addClass('navbar-dark brand-dark')
        $('#dark_css').attr('href', site_url + 'assets/t_dashboard/assets/css/layouts/dark.css')
        $('label[for="dark_mode"').css('background', '#3e3e3e')
    } else {
        $('#dark_css').attr('href', '')
        $('.pcoded-navbar').removeClass('navbar-dark brand-dark')
        $('label[for="dark_mode"').css('background', '#e3e3e3')
    }
}
toggleDark(localStorage.getItem('isDark'))
$('#dark_mode').on('change', (e) => {
    let isDark = e.currentTarget.checked
    toggleDark(isDark)
    if (isDark) {
        localStorage.setItem('isDark', true)
    } else {
        localStorage.setItem('isDark', false)
    }
})

// RESET FORM EACTH TIME MODAL IS CLOSED
$('.modal').on('hide.bs.modal', function () {
    if ($(this).find('form')[0]) $(this).find('form')[0].reset()
})

// ================== SELECT2 
// array must id & text
function select2_init(container, ntable, sid, sname, multiple, placehld, allowc = true) {
    $(container).select2({
        allowClear: allowc,
        placeholder: placehld,
        multiple: multiple,
        dropdownParent: $(container).parent(),
    });
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: site_url + 'api/convert_select2',
        data: {
            table: ntable,
            sid: sid,
            sname: sname,
        },
        success: function (response) {
            var $el = $(container);
            $el.empty(); // remove old options
            $el.append($("<option></option>"))
            for (i = 0; i < response.length; i++) {
                $el.append($("<option></option>")
                    .attr("value", response[i].id)
                    .text(response[i].text));
            }
        }
    });
}

function select2_init_manual(container, yourdata, multiple, placehld, url, allowc = true) {
    // console.log(yourdata)
    $(container).select2({
        allowClear: allowc,
        placeholder: placehld,
        multiple: multiple,
        dropdownParent: $(container).parent(),
    });

    var $el = $(container);
    $el.empty(); // remove old options
    $el.append($("<option></option>"))
    for (i = 0; i < yourdata.length; i++) {
        $el.append($("<option></option>")
            .attr("value", yourdata[i].id)
            .text(yourdata[i].text));
        // console.log(yourdata[i].text)
    }
}

function select2_pilih(container, dataSelect, multiple = false, allowc = true) {
    $(container).val(dataSelect).select2({
        allowClear: allowc,
        multiple: multiple,
        dropdownParent: $(container).parent()
    });
}

// select2 without url
function select2s_manual(container, multiple, placehld, allowc = false) {
    $(container).select2({
        multiple: multiple,
        placeholder: placehld,
        allowClear: allowc,
        dropdownParent: $(container).parent(),
    });
}

function getval(sel) {
    alert(sel.value);
}
// ================ DATE RANGE PICKIER
$('.drp_dateandtime_up').daterangepicker({
    timePicker: true,
    timePicker24Hour: true,
    timePickerIncrement: 1,
    locale: {
        format: 'DD/MM/YYYY HH:mm',
        cancelLabel: 'Clear'
    },
    singleDatePicker: true,
    showDropdowns: true,
    drops: "up",
    autoUpdateInput: false,
    // minDate: '01/01/2012',
    // maxDate: '12/31/2017'
});
$('.drp_dateandtime_up').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY HH:mm'));
});
$('.drp_dateandtime_up').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
});

$('.drp_dateandtime_down').daterangepicker({
    timePicker: true,
    timePicker24Hour: true,
    timePickerIncrement: 1,
    locale: {
        format: 'DD/MM/YYYY HH:mm',
        cancelLabel: 'Clear'
    },
    singleDatePicker: true,
    showDropdowns: true,
    drops: "down",
    autoUpdateInput: false,
    // minDate: '01/01/2012',
    // maxDate: '12/31/2017'
});
$('.drp_dateandtime_down').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY HH:mm'));
});
$('.drp_dateandtime_down').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
});

$('.drp_date_up').daterangepicker({
    timePicker: false,
    timePickerIncrement: 1,
    locale: {
        format: 'DD/MM/YYYY',
        cancelLabel: 'Clear'
    },
    singleDatePicker: true,
    showDropdowns: true,
    drops: "up",
    autoUpdateInput: false,
    // minDate: '01/01/2012',
    // maxDate: '12/31/2017'
});
$('.drp_date_up').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY'));
});
$('.drp_date_up').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
});

$('.drp_date_down').daterangepicker({
    timePicker: false,
    timePickerIncrement: 1,
    locale: {
        format: 'DD/MM/YYYY',
        cancelLabel: 'Clear'
    },
    singleDatePicker: true,
    showDropdowns: true,
    drops: "down",
    autoUpdateInput: false,
    // minDate: '01/01/2012',
    // maxDate: '12/31/2017'
});
$('.drp_date_down').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY'));
});
$('.drp_date_down').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
});

// ================ NONTIFY
function notify(type, message) {
    $.growl({
        message: message
    }, {
        type: type,
        allow_dismiss: true,
        label: 'Cancel',
        className: 'btn-xs btn-inverse',
        placement: {
            from: 'top',
            align: 'right'
        },
        delay: 2500,
        animate: {
            enter: 'animated bounceInRight',
            exit: 'animated bounceOutRight'
        },
        offset: {
            x: 30,
            y: 30
        }
    });
};

// ================= DROPZONE FUNCTION
function dropzone_init(container, your_url, ypost, max_file, max_size, accFiles, addRemovelink = true, sendtoid = false, idcontainer = '') {
    var myDropzone = new Dropzone(container, {
        url: your_url, // Set the url for your upload script location
        paramName: ypost, // The name that will be used to transfer the file
        maxFiles: max_file,
        maxFilesize: max_size, // MB
        addRemoveLinks: addRemovelink,
        acceptedFiles: accFiles,
        init: function () {
            this.on("processing", function (file) {

            });
            this.on("maxfilesexceeded", function (file) {
                this.removeAllFiles();
                this.addFile(file);
            });
            this.on('complete',
                function (file) {
                    // do something here
                });
            this.on("success", function (file, response, formData, xhr) {
                // do something here
                var obj = jQuery.parseJSON(response)
                if (sendtoid) {
                    $(idcontainer).val(obj.msg)
                }
            });
            this.on("error", function (data, errorMessage, xhr) {
                // do something here
            });

        },
        removedfile: function (file) {
            file.previewElement.remove();
            if (idcontainer) {
                var target = document.getElementById(idcontainer.replace('#', '')).value;
                if (target != '') {
                    delete_imgdz(target)
                }
                $(idcontainer).val()
            }
        }
    })
}

function delete_imgdz(path_img) {
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: site_url + 'api/remove_image',
        data: {
            target: path_img,
        },
        success: function (response) {

        }
    });
}